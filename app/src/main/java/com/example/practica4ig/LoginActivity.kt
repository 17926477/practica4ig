package com.example.practica4ig

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*

const val EXTRA_RFC = "com.example.practica4ig"

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (DataPreferences.getStoredRFC(this)  != "") {
            val  intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
    }

        fun clickRFC(view: View) {
        val Intent = Intent(this, MainActivity::class.java).apply {
            putExtra(EXTRA_RFC, txtRFC.text.toString())
        }

        startActivity(intent)
        finish()
    }
}

}



